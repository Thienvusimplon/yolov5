import streamlit as st
import pandas as pd
import torch
from PIL import Image

def load_image(image_file):
    img = Image.open(image_file)
    return img

def detect_label(image_file, model):
    number = 1
    predictions = model(load_image(image_file))
    st.write(predictions.pandas().xyxy[0])
    for i in range(len(predictions.crop())):
        label_confidence = predictions.crop()[i]["label"].split()       
        #if float(label_confidence[1]) >= 0.4: 
        st.subheader(f"Crop {number}")
        st.write("Label détecté :", label_confidence[0])
        st.write("Score de confidence : ", label_confidence[1])
        st.image(Image.fromarray(predictions.crop()[i]["im"]))
        number +=1
    st.image(Image.fromarray(predictions.render()[0]))

st.title("Detect réseau")

menu = ["A choisir", "Mètres", "Centimètres"]
choice = st.sidebar.selectbox("Menu",menu)

if choice == "A choisir":
    st.subheader("Choissisez l'unité de mesure des relevés dans le menu déroulant")

if choice == "Centimètres":
    st.subheader("Centimètres")
    image_file = st.file_uploader("Upload Images", type=["png","jpg","jpeg", "JPG"])
    model = torch.hub.load('/home/thienvu/yolov5', 'custom', path='app/centimetre.pt', force_reload=True, skip_validation=True, source='local')
    model.conf = 0.40
    if image_file != None:
        detect_label(image_file, model)

if choice == "Mètres":
    st.subheader("Mètres")
    image_file = st.file_uploader("Upload Images", type=["png","jpg","jpeg", "JPG"])
    model = torch.hub.load('/home/thienvu/yolov5', 'custom', path='app/metre.pt', force_reload=True, skip_validation=True, source='local')
    if image_file != None:
        detect_label(image_file, model)